from django.db import models

# Create your models here.


class FurnitureModel(models.Model):
    name = models.CharField(max_length=50)
    price = models.IntegerField()
    dimension = models.CharField(max_length=30)
    color = models.CharField(max_length=100)
    material = models.CharField(max_length=30)
    image = models.CharField(max_length=200)
