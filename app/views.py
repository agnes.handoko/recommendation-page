from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import JsonResponse
import json
import requests
import locale
from .models import FurnitureModel


# Create your views here.
response = {}


def homepage(request):
    return render(request, "recommendationPage.html")


def makedict(request):
    pointseveryfurniture = {}
    allFurniture = FurnitureModel.objects.all()
    viennaPrice = allFurniture[0].price
    viennaDimension = allFurniture[0].dimension
    viennaColor = allFurniture[0].color
    viennaColorSplit = viennaColor.split(", ")
    viennaMaterial = allFurniture[0].material
    # response['allFurniture'] = allFurniture

    # simpen indeksnya, urutin poinnya,
    # keknya bagusan kalo ake dict biar keynya poin fieldnya indeks,
    # terus ntar kalo udah diurutin bsa diretrieve,
    # jadi kek tinggal manggil sesuai indeks yg udah diurutin berdasarkan yg plg similar
    # print(allFurniture[0].name)

    for i in allFurniture:
        if i.name != 'Sofa 2 dudukan Vienna':
            pointseveryfurniture[i.name] = 0
            tempPoint = 0

            selisihHarga = abs(i.price - viennaPrice)
            tempPoint += selisihHarga

            if len(viennaDimension) == len(i.dimension):
                tempPoint += 1

            if viennaMaterial == i.material:
                tempPoint += 1

            colorSplit = i.color.split(", ")
            for j in viennaColorSplit:
                for k in colorSplit:
                    if j == k:
                        tempPoint += 1

            pointseveryfurniture[i.name] = tempPoint
    print(pointseveryfurniture)
    sort_orders = sorted(pointseveryfurniture.items(),
                         key=lambda x: x[1], reverse=False)
    print(sort_orders)
    print(sort_orders[0][0])
    furniturenamesorted = []
    for i in sort_orders:
        furniturenamesorted.append(i[0])
    print(furniturenamesorted)

    listofFurniture = []
    allFurniture = FurnitureModel.objects.all()

    locale.setlocale(locale.LC_ALL, '')

    for i in furniturenamesorted:
        for items in allFurniture:
            if i == items.name:
                newdict = {"name": items.name, "price": '{:n}'.format(items.price), "dimension": items.dimension,
                           "color": items.color, "material": items.material, "image": items.image}
                listofFurniture.append(newdict)
    print({'data': listofFurniture})
    print("panjang list", len(listofFurniture))
    return JsonResponse({'data': listofFurniture})
